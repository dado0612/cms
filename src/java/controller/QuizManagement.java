package controller;

import dao.QuizDAO;
import dao.ResultDAO;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpSession;
import model.Bank;
import model.Quiz;
import model.Result;

public class QuizManagement {

    private static final Map<String, Quiz> MAP = new TreeMap<>();

    public static Map<String, Quiz> getMAP() {
        return MAP;
    }

    public static void removeData(HttpSession ses, String username) {
        MAP.remove(username);
        ses.removeAttribute("num");
    }

    public static double finishQuiz(HttpSession ses) {
        double result = 0;
        String username = (String) ses.getAttribute("username");
        if (MAP.containsKey(username)) {
            List<Bank> listAnswer = MAP.get(username).getListBank();
            List<Integer> listBank = new QuizDAO().selectBank(listAnswer);
            int total = listAnswer.size();
            int count = 0;

            for (int i = 0; i < total; i++) {
                int ans = listAnswer.get(i).getAnswer();
                int res = listBank.get(i);
                if (ans == res) {
                    count++;
                }
            }

            result = count * 10.0 / total;
            Result res = new Result(username, result, new Date());
            new ResultDAO().addResult(res);
            removeData(ses, username);
        }
        return result;
    }

    public static void startQuiz(HttpSession ses, int num) {
        String username = (String) ses.getAttribute("username");
        List<Bank> list = new QuizDAO().createQuiz(num);
        long timer = list.size() * 60 * 1000;
        Date date = new Date();
        timer += date.getTime();
        date = new Date(timer);

        MAP.put(username, new Quiz(list, date));
        ses.setAttribute("num", String.valueOf(num));
    }

    public static List<String> contQuiz(String username, int questNo, int ans) {
        List<Bank> list = MAP.get(username).getListBank();
        if (questNo >= 0) {
            list.get(questNo).setAnswer(ans);
        }

        List<String> res = null;
        if (questNo < list.size() - 1) {
            Bank bank = list.get(questNo + 1);
            long questionID = bank.getQuestionID();
            int answer = bank.getAnswer();

            res = new QuizDAO().selectQuiz(questionID);
            res.add(String.valueOf(answer));
        }
        return res;
    }

    public static long getRemainingTime(String username) {
        Date date = MAP.get(username).getFinishTime();
        long timer = new Date().getTime();
        return (date.getTime() - timer) / 1000;
    }

    public static String getTime(String username) {
        long timer = getRemainingTime(username);
        if (timer < 0) {
            return "EXPIRE";
        } else {
            long min = timer / 60;
            long sec = timer % 60;

            StringBuilder re = new StringBuilder();
            re.append(min);
            re.append(":");

            if (sec < 10) {
                re.append("0");
            }
            re.append(sec);
            return re.toString();
        }
    }
}
