package servlet;

import dao.AccountDAO;
import dao.RoleDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Role;
import utils.EncodeUtils;
import utils.ServletUtils;
import utils.ValidUtils;

public class AddAccountServlet extends HttpServlet {

    private String isValidInput(String username, String password, String usertype, String email) {
        String msg;
        ValidUtils valid = new ValidUtils();

        msg = valid.isValidStr(username, "User Name");
        if (msg != null) {
            return msg;
        }

        msg = valid.isValidStr(password, "Password");
        if (msg != null) {
            return msg;
        }

        msg = valid.isValidStr(usertype, "User Type");
        if (msg != null) {
            return msg;
        }

        msg = valid.isValidStr(email, "Email");
        if (msg != null) {
            return msg;
        }

        msg = valid.isValidInt(usertype, "User Type", 1, 2);
        return msg;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletUtils util = new ServletUtils();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String usertype = request.getParameter("usertype");
        String email = request.getParameter("email");

        List<Role> list = new RoleDAO().selectAll();
        if (list == null) {
            new ServletUtils().sendMessage(request, response, "Internal server error");
            return;
        }

        String msg = isValidInput(username, password, usertype, email);
        if (msg == null) {
            int n = new AccountDAO().checkDuplicate(username);
            if (n == 0) {
                int roleID = Integer.valueOf(usertype);
                password = new EncodeUtils().encodeMD5(password);
                Account acc = new Account(username, password, roleID, email);

                new AccountDAO().addAccount(acc);
                util.sendMessage(request, response, "Register successfully");
                return;
            } else {
                msg = "Your username is in used";
            }
        }

        request.setAttribute("username", (username == null) ? "" : username);
        request.setAttribute("usertype", (usertype == null) ? 1 : Integer.valueOf(usertype));
        request.setAttribute("email", (email == null) ? "" : email);
        request.setAttribute("message", msg);
        request.setAttribute("list", list);
        request.getRequestDispatcher("jsp/register.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
