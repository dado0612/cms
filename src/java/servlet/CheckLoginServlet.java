package servlet;

import dao.AccountDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import utils.EncodeUtils;
import utils.ServletUtils;
import utils.ValidUtils;

public class CheckLoginServlet extends HttpServlet {

    private String isValidInput(String username, String password) {
        String msg;
        ValidUtils valid = new ValidUtils();

        msg = valid.isValidStr(username, "User Name");
        if (msg != null) {
            return msg;
        }

        msg = valid.isValidStr(password, "Password");
        return msg;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        String msg = isValidInput(username, password);
        if (msg == null) {
            password = new EncodeUtils().encodeMD5(password);
            Account acc = new Account(username, password);
            int n = new AccountDAO().checkLogin(acc);

            switch (n) {
                case -1:
                    new ServletUtils().sendMessage(request, response, "Internal server error");
                    return;
                case 0:
                    msg = "Wrong username or password";
                    break;
                default:
                    HttpSession session = request.getSession();
                    session.setAttribute("username", username);
                    response.sendRedirect("login");
                    return;
            }
        }

        request.setAttribute("username", (username == null) ? "" : username);
        request.setAttribute("message", msg);
        request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
