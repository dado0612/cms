package servlet;

import dao.QuestionDAO;
import dao.QuizDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.ConfigUtils;
import utils.ServletUtils;
import utils.ValidUtils;

public class ManageQuizServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletUtils util = new ServletUtils();
        String path = request.getServletPath();
        boolean check = util.checkSession(request, response, path);

        if (check) {
            HttpSession session = request.getSession();
            String username = (String) session.getAttribute("username");
            String num = request.getParameter("pageNo");
            int item = (int) new ConfigUtils().getConfig("PAGEITEM");

            String msg = "";
            String questID = request.getParameter("questID");
            if (questID != null) {
                try {
                    long id = Long.valueOf(questID);
                    int n = new QuizDAO().deleteQuiz(id);
                    if (n == 0) {
                        msg = "Question ID is not existed";
                    } else {
                        msg = "Delete successfully";
                    }
                } catch (NumberFormatException e) {
                    msg = "Question ID is invalid";
                }
            }

            QuestionDAO dao = new QuestionDAO();
            int count = dao.countByUsername(username);
            int max = (int) Math.ceil(count * 1.0 / item);
            int pageNo = new ValidUtils().validInt(num, 1, max);

            request.setAttribute("count", count);
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("maxPage", max);
            request.setAttribute("message", msg);
            request.setAttribute("list", dao.selectByUsername(username, pageNo));
            request.getRequestDispatcher("jsp/manageQuiz.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
