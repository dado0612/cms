package servlet;

import dao.QuizDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Answer;
import model.Bank;
import model.Question;
import utils.EncodeUtils;
import utils.ServletUtils;
import utils.ValidUtils;

public class AddQuizServlet extends HttpServlet {

    private String isValidOption(String[] arr) {
        String msg = null;
        ValidUtils valid = new ValidUtils();

        if (arr == null || arr.length != 4) {
            return "Each question must have enough 4 options";
        }

        int len = arr.length;
        for (int i = 0; i < len; i++) {
            String val = arr[i];
            String name = "Option " + (i + 1);
            msg = valid.isValidStr(val, name);
            if (msg != null) {
                return msg;
            }
        }
        return msg;
    }

    private String isValidAnswer(String[] arr) {
        String msg = null;
        ValidUtils valid = new ValidUtils();

        if (arr == null) {
            msg = "Each question must have at least 1 answer";
        } else {
            if (arr.length == 4) {
                return "Question can't get all option as answer";
            }

            Set<Integer> set = new HashSet<>();
            int len = arr.length;
            for (int i = 0; i < len; i++) {
                String val = arr[i];
                String name = "Answer in option " + (i + 1);
                msg = valid.isValidInt(val, name, 1, 4);
                if (msg != null) {
                    return msg;
                }
                set.add(Integer.valueOf(val));
            }

            if (set.size() != arr.length) {
                msg = "Each answer must be different from each other";
            }
        }
        return msg;
    }

    private String isValidInput(String quest, String[] arrOpt, String[] arrAns) {
        String msg;
        ValidUtils valid = new ValidUtils();

        msg = valid.isValidStr(quest, "Question");
        if (msg != null) {
            return msg;
        }

        msg = isValidOption(arrOpt);
        if (msg != null) {
            return msg;
        }

        msg = isValidAnswer(arrAns);
        return msg;
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String path = "/makeQuiz";
        boolean check = new ServletUtils().checkSession(request, response, path);

        if (check) {
            EncodeUtils encode = new EncodeUtils();
            String quest = request.getParameter("question");
            String[] arrOpt = request.getParameterValues("option");
            String[] arrAns = request.getParameterValues("answer");

            String msg = isValidInput(quest, arrOpt, arrAns);
            if (msg == null) {
                HttpSession session = request.getSession();
                String username = (String) session.getAttribute("username");
                Date date = new Date();
                long timer = date.getTime();

                quest = encode.encodeHTML(quest);
                Question question = new Question(timer, quest, username, date);
                Bank bank = new Bank(timer, encode.encodeAnswer(arrAns));
                List<Answer> list = new ArrayList<>();

                timer *= 10;
                for (String opt : arrOpt) {
                    opt = encode.encodeHTML(opt);
                    list.add(new Answer(++timer, opt));
                }

                new QuizDAO().addQuiz(question, list, bank);
                new ServletUtils().sendMessage(request, response, "Add quiz successfully");
            } else {
                String[] arr = new String[4];
                Arrays.fill(arr, "");
                if (arrOpt != null) {
                    int len = arrOpt.length;
                    System.arraycopy(arrOpt, 0, arr, 0, len);
                }

                request.setAttribute("question", (quest == null) ? "" : quest);
                request.setAttribute("option", arr);
                request.setAttribute("message", msg);
                request.getRequestDispatcher("jsp/makeQuiz.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
