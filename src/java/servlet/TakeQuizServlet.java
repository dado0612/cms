package servlet;

import controller.QuizManagement;
import dao.QuestionDAO;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.EncodeUtils;
import utils.ServletUtils;
import utils.ValidUtils;

public class TakeQuizServlet extends HttpServlet {

    private void getNextQuest(HttpServletRequest request, HttpServletResponse response,
            int questNo, List<String> list) throws ServletException, IOException {
        request.setAttribute("list", list);
        request.setAttribute("questNo", questNo + 1);
        request.getRequestDispatcher("jsp/takeQuiz.jsp").forward(request, response);
    }

    private void destroyQuiz(HttpServletRequest request, HttpServletResponse response,
            String num) throws ServletException, IOException {
        double result = QuizManagement.finishQuiz(request.getSession());
        request.setAttribute("result", result);
        request.setAttribute("num", num);
        request.setAttribute("percent", Math.round(result * 10));
        request.getRequestDispatcher("jsp/finishQuiz.jsp").forward(request, response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServletUtils util = new ServletUtils();
        ValidUtils valid = new ValidUtils();
        String path = request.getServletPath();
        boolean check = util.checkSession(request, response, path);

        if (check) {
            String numReq = request.getParameter("num");
            HttpSession session = request.getSession();
            String numSes = (String) session.getAttribute("num");
            String username = (String) session.getAttribute("username");
            int max = new QuestionDAO().countAll();

            if (numReq != null) {
                String msg = valid.isValidInt(numReq, "Number of question ", 1, max);
                if (msg == null) {
                    int num = Integer.valueOf(numReq);
                    QuizManagement.finishQuiz(session);
                    QuizManagement.startQuiz(session, num);
                    List<String> list = QuizManagement.contQuiz(username, -1, 0);
                    getNextQuest(request, response, -1, list);
                } else {
                    util.sendMessage(request, response, msg);
                }
            } else {
                if (numSes != null) {
                    String no = request.getParameter("questNo");
                    int num = Integer.valueOf(numSes) - 1;
                    int questNo = valid.validInt(no, -1, num);
                    String[] arr = request.getParameterValues("answer");
                    int ans = new EncodeUtils().encodeAnswer(arr);

                    String timer = QuizManagement.getTime(username);
                    if (timer.equals("EXPIRE")) {
                        QuizManagement.removeData(session, username);
                        String s = "Time limit exceeded";
                        util.sendMessage(request, response, s);
                    } else {
                        List<String> list = QuizManagement.contQuiz(username, questNo, ans);
                        if (list == null) {
                            destroyQuiz(request, response, numSes);
                        } else {
                            getNextQuest(request, response, questNo, list);
                        }
                    }
                } else {
                    request.setAttribute("max", max);
                    request.getRequestDispatcher("jsp/startQuiz.jsp").forward(request, response);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
