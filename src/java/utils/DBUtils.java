package utils;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBUtils {

    private final String serverName;
    private final String dbName;
    private final int portNumber;
    private final String userID;
    private final String password;

    public DBUtils() {
        ConfigUtils config = new ConfigUtils();
        serverName = (String) config.getConfig("SERVERNAME");
        dbName = (String) config.getConfig("DBNAME");
        portNumber = (int) config.getConfig("PORTNUMBER");
        userID = (String) config.getConfig("USERID");
        password = (String) config.getConfig("PASSWORD");
    }

    public Connection getConnection() throws Exception {
        String url = "jdbc:sqlserver://" + serverName + ":" + portNumber + ";databaseName=" + dbName;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection(url, userID, password);
    }
}
