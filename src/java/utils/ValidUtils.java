package utils;

public class ValidUtils {

    public int validInt(String num, int min, int max) {
        int re;
        try {
            re = Integer.valueOf(num);
        } catch (NumberFormatException e) {
            re = min;
        }

        re = Math.max(re, min);
        return Math.min(re, max);
    }

    public String isValidInt(String num, String name, int min, int max) {
        String msg = null;
        try {
            int re = Integer.valueOf(num);
            if (re < min || re > max) {
                msg = name + " is out of bound";
            }
        } catch (NumberFormatException e) {
            msg = name + " is not an integer";
        }
        return msg;
    }

    public String isValidStr(String val, String name) {
        String msg = null;
        if (val == null) {
            msg = name + " field is invalid";
        } else {
            if (val.trim().equals("")) {
                msg = name + " field is empty";
            }
        }
        return msg;
    }
}
