package utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class EncodeUtils {

    public String encodeMD5(String pass) {
        String re = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] arr = md.digest(pass.getBytes());
            BigInteger num = new BigInteger(1, arr);
            re = num.toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return String.format("%32s", re).replace(' ', '0');
    }

    public int encodeAnswer(String[] arr) {
        if (arr == null) {
            return 0;
        } else {
            int re = 0;
            for (String s : arr) {
                re = re * 10 + Integer.valueOf(s);
            }
            return re;
        }
    }

    public List<Integer> decodeAnswer(int ans) {
        List<Integer> list = new ArrayList<>();
        while (ans != 0) {
            list.add(ans % 10);
            ans /= 10;
        }
        return list;
    }

    public String encodeHTML(String content) {
        StringBuilder re = new StringBuilder();
        String[] arr = content.split("\\n");
        for (String s : arr) {
            re.append(s.trim());
            re.append("<br />");
        }
        return re.toString();
    }
}
