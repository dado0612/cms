package utils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class ConfigUtils {

    public Object getConfig(String name) {
        Object obj = null;
        try {
            Context init = new InitialContext();
            Context ctx = (Context) init.lookup("java:comp/env");
            obj = ctx.lookup(name);
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
