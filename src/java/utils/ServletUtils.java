package utils;

import dao.AccountDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletUtils {

    public void sendMessage(HttpServletRequest req, HttpServletResponse res,
            String msg) throws ServletException, IOException {
        req.setAttribute("message", msg);
        req.getRequestDispatcher("jsp/message.jsp").forward(req, res);
    }

    public boolean checkSession(HttpServletRequest req, HttpServletResponse res,
            String path) throws ServletException, IOException {
        boolean check = false;
        HttpSession session = req.getSession();
        String username = (String) session.getAttribute("username");

        if (username != null) {
            int n = new AccountDAO().checkAuthor(username, path);
            switch (n) {
                case -1:
                    sendMessage(req, res, "Internal server error");
                    break;
                case 0:
                    sendMessage(req, res, "You are not authorization");
                    break;
                default:
                    check = true;
            }
        } else {
            sendMessage(req, res, "You are not login");
        }
        return check;
    }
}
