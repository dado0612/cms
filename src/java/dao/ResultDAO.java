package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import model.Result;
import utils.DBUtils;

public class ResultDAO {

    private final DBUtils context;

    public ResultDAO() {
        context = new DBUtils();
    }

    public int addResult(Result res) {
        int n;
        String sql = "INSERT INTO Result VALUES (?,?,?)";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, res.getUsername());
            ps.setDouble(2, res.getResult());
            ps.setTimestamp(3, new Timestamp(res.getDateCreate().getTime()));
            n = ps.executeUpdate();
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }
}
