package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Answer;
import model.Bank;
import model.Question;
import utils.DBUtils;

public class QuizDAO {

    private final DBUtils context;

    public QuizDAO() {
        context = new DBUtils();
    }

    private void addQuestion(Connection con, Question quest) throws Exception {
        String sql = "INSERT INTO Question VALUES (?,?,?,?)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, quest.getQuestionID());
            ps.setString(2, quest.getQuestionContent());
            ps.setString(3, quest.getUsername());
            ps.setTimestamp(4, new Timestamp(quest.getDateCreate().getTime()));
            ps.executeUpdate();
        }
    }

    private void addAnswer(Connection con, List<Answer> list) throws Exception {
        String sql = "INSERT INTO Answer VALUES (?,?)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            for (Answer ans : list) {
                ps.setLong(1, ans.getAnswerID());
                ps.setString(2, ans.getAnswerContent());
                ps.addBatch();
            }
            ps.executeBatch();
        }
    }

    private void addBank(Connection con, Bank bank) throws Exception {
        String sql = "INSERT INTO Bank VALUES (?,?)";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, bank.getQuestionID());
            ps.setInt(2, bank.getAnswer());
            ps.executeUpdate();
        }
    }

    public int addQuiz(Question quest, List<Answer> list, Bank bank) {
        int n = 0;
        try (Connection con = context.getConnection()) {
            try {
                con.setAutoCommit(false);
                addQuestion(con, quest);
                addAnswer(con, list);
                addBank(con, bank);
                con.commit();
            } catch (Exception e) {
                con.rollback();
                n = -1;
            }
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }

    private int deleteQuestion(Connection con, long questionID) throws Exception {
        int n;
        String sql = "DELETE FROM Question WHERE questionID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, questionID);
            n = ps.executeUpdate();
        }
        return n;
    }

    private void deleteAnswer(Connection con, long questionID) throws Exception {
        String id = questionID + "_";
        String sql = "DELETE FROM Answer WHERE answerID LIKE ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, id);
            ps.executeUpdate();
        }
    }

    public int deleteQuiz(long questionID) {
        int n;
        try (Connection con = context.getConnection()) {
            try {
                con.setAutoCommit(false);
                deleteAnswer(con, questionID);
                n = deleteQuestion(con, questionID);
                con.commit();
            } catch (Exception e) {
                con.rollback();
                n = -1;
            }
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }

    private String selectQuestion(Connection con, long questionID) throws Exception {
        String re = null;
        String sql = "SELECT questionContent FROM Question WHERE questionID = ?";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setLong(1, questionID);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    re = rs.getString("questionContent");
                }
            }
        }
        return re;
    }

    private List<String> selectAnswer(Connection con, long questionID) throws Exception {
        String id = questionID + "_";
        List<String> list = new ArrayList<>();
        String sql = "SELECT answerContent FROM Answer WHERE answerID LIKE ? ORDER BY answerID";
        try (PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    list.add(rs.getString("answerContent"));
                }
            }
        }
        return list;
    }

    public List<String> selectQuiz(long questionID) {
        List<String> list = new ArrayList<>();
        try (Connection con = context.getConnection()) {
            list.add(selectQuestion(con, questionID));
            list.addAll(selectAnswer(con, questionID));
        } catch (Exception e) {
            list = null;
        }
        return list;
    }

    public List<Bank> createQuiz(int num) {
        List<Bank> list = new ArrayList<>();
        String sql = "SELECT TOP(?) questionID FROM Question ORDER BY NEWID()";

        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, num);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    long questionID = rs.getLong("questionID");
                    list.add(new Bank(questionID, 0));
                }
            }
        } catch (Exception e) {
            list = null;
        }
        return list;
    }

    public List<Integer> selectBank(List<Bank> listBank) {
        List<Integer> list = new ArrayList<>();
        String sql = "SELECT answer FROM Bank WHERE questionID = ?";

        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            for (Bank bank : listBank) {
                ps.setLong(1, bank.getQuestionID());
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        list.add(rs.getInt("answer"));
                    }
                }
            }
        } catch (Exception e) {
            list = null;
        }
        return list;
    }
}
