package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Account;
import utils.DBUtils;

public class AccountDAO {

    private final DBUtils context;

    public AccountDAO() {
        context = new DBUtils();
    }

    public int checkLogin(Account acc) {
        int n = 0;
        String sql = "SELECT username FROM Account WHERE username = ? AND password = ?";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, acc.getUsername());
            ps.setString(2, acc.getPassword());

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    n = 1;
                }
            }
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }

    public int checkDuplicate(String username) {
        int n = 0;
        String sql = "SELECT username FROM Account WHERE username = ?";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    n = 1;
                }
            }
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }

    public int addAccount(Account acc) {
        int n;
        String sql = "INSERT INTO Account VALUES (?,?,?,?)";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, acc.getUsername());
            ps.setString(2, acc.getPassword());
            ps.setInt(3, acc.getRoleID());
            ps.setString(4, acc.getEmail());
            n = ps.executeUpdate();
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }

    public int checkAuthor(String username, String path) {
        int n = 0;
        String sql = "SELECT Account.username FROM Page INNER JOIN\n"
                + "Authority ON Page.pageID = Authority.pageID INNER JOIN\n"
                + "Role ON Authority.roleID = Role.roleID INNER JOIN\n"
                + "Account ON Role.roleID = Account.roleID\n"
                + "WHERE username = ? AND pageURL = ?";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            ps.setString(2, path);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    n = 1;
                }
            }
        } catch (Exception e) {
            n = -1;
        }
        return n;
    }
}
