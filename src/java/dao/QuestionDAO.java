package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Question;
import utils.ConfigUtils;
import utils.DBUtils;

public class QuestionDAO {

    private final DBUtils context;

    public QuestionDAO() {
        context = new DBUtils();
    }

    public int countByUsername(String username) {
        int num = 0;
        String sql = "SELECT COUNT(questionID) FROM Question WHERE username = ?";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    num = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            num = -1;
        }
        return num;
    }

    public int countAll() {
        int num = 0;
        String sql = "SELECT COUNT(questionID) FROM Question";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    num = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            num = -1;
        }
        return num;
    }

    public List<Question> selectByUsername(String username, int pageNo) {
        List<Question> list = new ArrayList<>();
        int pageItem = (int) new ConfigUtils().getConfig("PAGEITEM");
        int from = (pageNo - 1) * pageItem + 1;
        int to = pageNo * pageItem;

        String sql = "WITH tbl AS (SELECT *,ROW_NUMBER()\n"
                + "OVER (ORDER BY questionID) as rowno\n"
                + "FROM Question WHERE username = ?)\n"
                + "SELECT * FROM tbl WHERE rowno BETWEEN ? AND ?";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setString(1, username);
            ps.setInt(2, from);
            ps.setInt(3, to);

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    long questionID = rs.getLong("questionID");
                    String questionContent = rs.getString("questionContent");
                    Date dateCreate = rs.getTimestamp("dateCreate");
                    list.add(new Question(questionID, questionContent, username, dateCreate));
                }
            }
        } catch (Exception e) {
            list = null;
        }
        return list;
    }
}
