package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Role;
import utils.DBUtils;

public class RoleDAO {

    private final DBUtils context;

    public RoleDAO() {
        context = new DBUtils();
    }

    public List<Role> selectAll() {
        List<Role> list = new ArrayList<>();
        String sql = "SELECT * FROM Role";
        try (Connection con = context.getConnection();
                PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    int roleID = rs.getInt("roleID");
                    String roleName = rs.getString("roleName");
                    String description = rs.getString("description");
                    list.add(new Role(roleID, roleName, description));
                }
            }
        } catch (Exception e) {
            list = null;
        }
        return list;
    }
}
