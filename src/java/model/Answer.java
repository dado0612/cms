package model;

public class Answer {

    private long answerID;
    private String answerContent;

    public Answer(long answerID, String answerContent) {
        this.answerID = answerID;
        this.answerContent = answerContent;
    }

    public long getAnswerID() {
        return answerID;
    }

    public void setAnswerID(long answerID) {
        this.answerID = answerID;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    @Override
    public String toString() {
        return "Answer{" + "answerID=" + answerID + ", answerContent=" + answerContent + '}';
    }
}
