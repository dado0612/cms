package model;

import java.util.Date;

public class Question {

    private long questionID;
    private String questionContent;
    private String username;
    private Date dateCreate;

    public Question(long questionID, String questionContent, String username, Date dateCreate) {
        this.questionID = questionID;
        this.questionContent = questionContent;
        this.username = username;
        this.dateCreate = dateCreate;
    }

    public long getQuestionID() {
        return questionID;
    }

    public void setQuestionID(long questionID) {
        this.questionID = questionID;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    @Override
    public String toString() {
        return "Question{" + "questionID=" + questionID + ", questionContent=" + questionContent + ", username=" + username + ", dateCreate=" + dateCreate + '}';
    }
}
