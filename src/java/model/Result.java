package model;

import java.util.Date;

public class Result {

    private int resultID;
    private String username;
    private double result;
    private Date dateCreate;

    public Result(String username, double result, Date dateCreate) {
        this.username = username;
        this.result = result;
        this.dateCreate = dateCreate;
    }

    public int getResultID() {
        return resultID;
    }

    public void setResultID(int resultID) {
        this.resultID = resultID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    @Override
    public String toString() {
        return "Result{" + "resultID=" + resultID + ", username=" + username + ", result=" + result + ", dateCreate=" + dateCreate + '}';
    }
}
