package model;

public class Bank {

    private int bankID;
    private long questionID;
    private int answer;

    public Bank(long questionID, int answer) {
        this.questionID = questionID;
        this.answer = answer;
    }

    public int getBankID() {
        return bankID;
    }

    public void setBankID(int bankID) {
        this.bankID = bankID;
    }

    public long getQuestionID() {
        return questionID;
    }

    public void setQuestionID(long questionID) {
        this.questionID = questionID;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "Bank{" + "bankID=" + bankID + ", questionID=" + questionID + ", answer=" + answer + '}';
    }
}
