package model;

import java.util.Date;
import java.util.List;

public class Quiz {

    private List<Bank> listBank;
    private Date finishTime;

    public Quiz(List<Bank> listBank, Date finishTime) {
        this.listBank = listBank;
        this.finishTime = finishTime;
    }

    public List<Bank> getListBank() {
        return listBank;
    }

    public void setListBank(List<Bank> listBank) {
        this.listBank = listBank;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }
}
