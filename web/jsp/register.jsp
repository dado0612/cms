<%@page import="model.Role"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String username = (String) request.getAttribute("username");
    int usertype = (int) request.getAttribute("usertype");
    String email = (String) request.getAttribute("email");
    String msg = (String) request.getAttribute("message");
    List<Role> list = (List) request.getAttribute("list");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div class="header form">
                    Registration Form
                </div>

                <form method="POST" action="addAccount">
                    <div class="main">
                        <div class="right">
                            <div class="main">
                                <div class="left center content">
                                    User Name:
                                </div>
                                <div class="right">
                                    <input type="text" name="username" value="<%= username%>" required />
                                </div>
                            </div>

                            <div class="main">
                                <div class="left center content">
                                    Password:
                                </div>
                                <div class="right">
                                    <input type="password" name="password" required />
                                </div>
                            </div>

                            <div class="main">
                                <div class="left center content">
                                    User Type:
                                </div>
                                <div class="right">
                                    <select name="usertype">
                                        <%
                                            for (Role role : list) {
                                                int id = role.getRoleID();
                                        %>
                                        <option value="<%= id%>" <%= usertype == id ? "selected" : ""%>
                                                ><%= role.getRoleName()%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </div>
                            </div>

                            <div class="main">
                                <div class="left center content">
                                    Email:
                                </div>
                                <div class="right">
                                    <input type="email" name="email" value="<%= email%>" required />
                                </div>
                            </div>

                            <div class="main">
                                <div class="left"></div>
                                <div class="right">
                                    <input type="submit" value="Register" />
                                </div>
                            </div>
                        </div>
                        <div class="left"></div>
                    </div>
                </form>

                <div class="header"></div>
                <div class="clock">
                    <%= msg%>
                </div>
            </div>
        </div>
    </body>
</html>
