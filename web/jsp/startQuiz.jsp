<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String username = (String) session.getAttribute("username");
    int max = (int) request.getAttribute("max");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Take Quiz</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div class="header">
                    <span class="content">
                        Welcome
                    </span>
                    <span class="title">
                        <%= username%>
                    </span>
                </div>

                <div class="content">
                    Enter number of questions:
                </div>

                <form method="POST" action="takeQuiz">
                    <div>
                        <input type="number" name="num" min="1" max="<%= max%>" required />
                    </div>

                    <div class="main">
                        <div class="left"></div>
                        <div class="right">
                            <input type="submit" value="Start" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
