<%@page import="utils.EncodeUtils"%>
<%@page import="controller.QuizManagement"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String num = (String) session.getAttribute("num");
    int max = Integer.valueOf(num) - 1;
    String username = (String) session.getAttribute("username");
    int questNo = (int) request.getAttribute("questNo");
    List<String> list = (List) request.getAttribute("list");

    String timer = QuizManagement.getTime(username);
    long countdown = QuizManagement.getRemainingTime(username) + 1;
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Take Quiz</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <meta http-equiv="refresh" content="<%= countdown%>">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div>
                    <span class="content">
                        Welcome
                    </span>
                    <span class="title">
                        <%= username%>
                    </span>
                </div>

                <div class="main header">
                    <div class="mid"></div>
                    <div class="mid">
                        <span class="content">
                            Time remaining
                        </span>
                        <span class="clock" id="clock">
                            <%= timer%>
                        </span>
                    </div>
                </div>

                <form method="POST" action="takeQuiz">
                    <div class="content">
                        <div><%= list.get(0)%></div>

                        <%
                            int ans = Integer.valueOf(list.get(5));
                            List<Integer> listAns = new EncodeUtils().decodeAnswer(ans);
                            for (int i = 1; i < 5; i++) {
                                boolean check = listAns.contains(i);
                        %>
                        <div>
                            <input type="checkbox" name="answer" value="<%= i%>"
                                   <%= check ? "checked" : ""%> /><%= list.get(i)%>
                        </div>
                        <%
                            }
                        %>
                    </div>

                    <div class="main">
                        <div class="mright"></div>
                        <div class="mleft">
                            <input type="hidden" name="questNo" value="<%= questNo%>" />
                            <input type="submit" value="<%= (questNo == max) ? "Finish" : "Next"%>" />
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <script src="js/clock.js"></script>
    </body>
</html>
