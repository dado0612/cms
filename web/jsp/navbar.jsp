<%
    String username = (String) session.getAttribute("username");
%>

<div class="navbar">
    <ul>
        <li><a href="login">Home</a></li>
        <li><a href="takeQuiz">Take Quiz</a></li>
        <li><a href="makeQuiz">Make Quiz</a></li>
        <li><a href="manageQuiz">Manage Quiz</a></li>

        <%
            if (username != null) {
        %>
        <li><a href="logout">Log out</a></li>
            <%
                }
            %>
    </ul>
</div>