<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String username = (String) request.getAttribute("username");
    String msg = (String) request.getAttribute("message");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div class="header form">
                    Login Form
                </div>

                <form method="POST" action="checkLogin">
                    <div class="main">
                        <div class="mid">
                            <div class="main">
                                <div class="left center content">
                                    User Name:
                                </div>
                                <div class="right">
                                    <input type="text" name="username" value="<%= username%>" required />
                                </div>
                            </div>

                            <div class="main">
                                <div class="left center content">
                                    Password:
                                </div>
                                <div class="right">
                                    <input type="password" name="password" required />
                                </div>
                            </div>

                            <div class="main">
                                <div class="left"></div>
                                <div class="right">
                                    <input type="submit" value="Sign in" />
                                    <a href="register" class="content">Register</a>
                                </div>
                            </div>
                        </div>
                        <div class="mid"></div>
                    </div>
                </form>

                <div class="header"></div>
                <div class="clock">
                    <%= msg%>
                </div>
            </div>
        </div>
    </body>
</html>
