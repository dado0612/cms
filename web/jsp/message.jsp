<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String msg = (String) request.getAttribute("message");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Message Page</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar content">
                <h2><%= msg%></h2>
            </div>
        </div>
    </body>
</html>
