<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%@page import="model.Question"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    int count = (int) request.getAttribute("count");
    int pageNo = (int) request.getAttribute("pageNo");
    int maxPage = (int) request.getAttribute("maxPage");
    String msg = (String) request.getAttribute("message");
    List<Question> list = (List) request.getAttribute("list");
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Quiz</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div class="header">
                    <span class="content">
                        Number of questions:
                    </span>
                    <span class="title">
                        <%= count%>
                    </span>
                </div>

                <div class="main title">
                    <div class="right">
                        Question
                    </div>
                    <div class="left">
                        Date Created
                    </div>
                </div>

                <%
                    for (Question quest : list) {
                %>
                <div class="main content">
                    <div class="right center">
                        <%= quest.getQuestionContent()%>
                    </div>
                    <div class="left center">
                        <div class="main">
                            <div class="right center">
                                <%= formatter.format(quest.getDateCreate())%>
                            </div>
                            <div class="left">
                                <form method="POST" action="manageQuiz">
                                    <input type="hidden" name="questID" value="<%= quest.getQuestionID()%>" />
                                    <input type="hidden" name="pageNo" value="<%= pageNo%>" />
                                    <input type="submit" value="Delete" />
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <%
                    }
                %>

                <div class="header"></div>

                <div class="main">
                    <div class="right"></div>
                    <div class="left">
                        <div class="main">
                            <div class="left">
                                <%
                                    if (pageNo != 1) {
                                %>
                                <form method="POST" action="manageQuiz">
                                    <input type="hidden" name="pageNo" value="<%= pageNo - 1%>" />
                                    <input type="submit" value="Prev" />
                                </form>
                                <%
                                    }
                                %>
                            </div>

                            <div class="left center title">
                                <%= pageNo%> of <%= maxPage%>
                            </div>

                            <div class="left">
                                <%
                                    if (pageNo != maxPage) {
                                %>
                                <form method="POST" action="manageQuiz">
                                    <input type="hidden" name="pageNo" value="<%= pageNo + 1%>" />
                                    <input type="submit" value="Next" />
                                </form>
                                <%
                                    }
                                %>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clock">
                    <%= msg%>
                </div>
            </div>
        </div>
    </body>
</html>
