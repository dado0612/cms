<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String quest = (String) request.getAttribute("question");
    String[] arrOpt = (String[]) request.getAttribute("option");
    String msg = (String) request.getAttribute("message");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Make Quiz</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/makeQuiz.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <form method="POST" action="addQuiz">
                    <div class="main">
                        <div class="mleft content">
                            Question:
                        </div>
                        <div class="mright">
                            <textarea name="question" class="question"
                                      required><%= quest%></textarea>
                        </div>
                    </div>

                    <%
                        for (int i = 1; i < 5; i++) {
                    %>
                    <div class="main">
                        <div class="mleft content">
                            Option <%= i%>:
                        </div>
                        <div class="mright">
                            <textarea name="option" class="option"
                                      required><%= arrOpt[i - 1]%></textarea>
                        </div>
                    </div>
                    <%
                        }
                    %>

                    <div class="main content">
                        <div class="mleft">
                            Answer(s):
                        </div>
                        <div class="mright">
                            <input type="checkbox" name="answer" value="1" />Option 1
                            <input type="checkbox" name="answer" value="2" />Option 2
                            <input type="checkbox" name="answer" value="3" />Option 3
                            <input type="checkbox" name="answer" value="4" />Option 4
                        </div>
                    </div>

                    <div class="main">
                        <div class="mleft"></div>
                        <div class="mright">
                            <input type="submit" value="Save" />
                        </div>
                    </div>
                </form>

                <div class="header"></div>
                <div class="clock">
                    <%= msg%>
                </div>
            </div>
        </div>
    </body>
</html>
