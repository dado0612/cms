<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String num = (String) request.getAttribute("num");
    double result = (double) request.getAttribute("result");
    long percent = (long) request.getAttribute("percent");
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Take Quiz</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>

    <body>
        <div class="frame">
            <jsp:include page="navbar.jsp" />

            <div class="mainbar">
                <div class="header">
                    <span class="content">
                        Your score:
                    </span>
                    <span class="title">
                        <%= String.format("%.1f", result)%>
                        (<%= percent%>%) -
                    </span>

                    <%
                        if (result < 4) {
                    %>
                    <span class="clock">
                        Not Pass
                    </span>
                    <%
                    } else {
                    %>
                    <span class="title">
                        Passed
                    </span>
                    <%
                        }
                    %>
                </div>

                <form method="POST" action="takeQuiz">
                    <span class="content">
                        Take another test
                    </span>

                    <input type="hidden" name="num" value="<%= num%>" />
                    <input type="submit" value="Start" />
                </form>
            </div>
        </div>
    </body>
</html>
