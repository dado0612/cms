{
    var element = document.getElementById("clock");
    var timer = element.textContent.split(":");
    var pivot = new Date().getTime();

    pivot += Number.parseInt(timer[0]) * 60 * 1000;
    pivot += Number.parseInt(timer[1]) * 1000;
    setInterval(countdown, 100);
}

function countdown() {
    var now = new Date().getTime();
    var diff = Math.round(pivot - now) / 1000;

    if (diff < 0) {
        element.innerHTML = "EXPIRE";
    } else {
        var min = Math.floor(diff / 60);
        var sec = Math.floor(diff % 60);
        if (sec < 10) {
            sec = "0" + sec;
        }
        element.innerHTML = min + ":" + sec;
    }
}