USE [master]
GO
/****** Object:  Database [CMS]    Script Date: 13/02/2019 9:22:30 ******/
CREATE DATABASE [CMS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CMS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CMS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CMS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CMS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CMS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CMS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CMS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CMS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CMS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CMS] SET ARITHABORT OFF 
GO
ALTER DATABASE [CMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CMS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CMS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CMS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CMS] SET RECOVERY FULL 
GO
ALTER DATABASE [CMS] SET  MULTI_USER 
GO
ALTER DATABASE [CMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CMS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CMS] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CMS] SET QUERY_STORE = OFF
GO
USE [CMS]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[username] [varchar](255) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[roleID] [int] NOT NULL,
	[email] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Answer]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answer](
	[answerID] [bigint] NOT NULL,
	[answerContent] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[answerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Authority]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Authority](
	[authorID] [int] IDENTITY(1,1) NOT NULL,
	[roleID] [int] NOT NULL,
	[pageID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[authorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bank]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bank](
	[bankID] [int] IDENTITY(1,1) NOT NULL,
	[questionID] [bigint] NOT NULL,
	[answer] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[bankID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Page]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[pageID] [int] IDENTITY(1,1) NOT NULL,
	[pageURL] [varchar](255) NOT NULL,
	[description] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[pageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Question]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[questionID] [bigint] NOT NULL,
	[questionContent] [text] NOT NULL,
	[username] [varchar](255) NOT NULL,
	[dateCreate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[questionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Result]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Result](
	[resultID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](255) NOT NULL,
	[result] [float] NOT NULL,
	[dateCreate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[resultID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 13/02/2019 9:22:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[roleName] [varchar](255) NOT NULL,
	[description] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[Account] ([username], [password], [roleID], [email]) VALUES (N'hailt', N'c4ca4238a0b923820dcc509a6f75849b', 1, N'hailt@fe.edu.vn')
INSERT [dbo].[Account] ([username], [password], [roleID], [email]) VALUES (N'quynhnd', N'c4ca4238a0b923820dcc509a6f75849b', 2, N'quynhnd@fpt.edu.vn')
INSERT [dbo].[Account] ([username], [password], [roleID], [email]) VALUES (N'tuanvm', N'c4ca4238a0b923820dcc509a6f75849b', 1, N'tuanvm@fe.edu.vn')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242109041, N'mot')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242109042, N'hai')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242109043, N'ba')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242109044, N'bon')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242210191, N'mot')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242210192, N'hai')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242210193, N'ba')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242210194, N'bon')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242310851, N'mot')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242310852, N'hai')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242310853, N'ba')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242310854, N'bon')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242411511, N'mot')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242411512, N'hai')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242411513, N'ba')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242411514, N'bon')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242512141, N'nam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242512142, N'sau')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242512143, N'bay')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242512144, N'tam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242612771, N'nam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242612772, N'sau')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242612773, N'bay')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242612774, N'tam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242713491, N'nam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242713492, N'sau')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242713493, N'bay')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242713494, N'tam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242814171, N'nam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242814172, N'sau')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242814173, N'bay')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500242814174, N'tam')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500244471621, N'1<br />')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500244471622, N'2<br />')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500244471623, N'3<br />')
INSERT [dbo].[Answer] ([answerID], [answerContent]) VALUES (15500244471624, N'4<br />')
SET IDENTITY_INSERT [dbo].[Authority] ON 

INSERT [dbo].[Authority] ([authorID], [roleID], [pageID]) VALUES (1, 1, 1)
INSERT [dbo].[Authority] ([authorID], [roleID], [pageID]) VALUES (2, 1, 2)
INSERT [dbo].[Authority] ([authorID], [roleID], [pageID]) VALUES (3, 1, 3)
INSERT [dbo].[Authority] ([authorID], [roleID], [pageID]) VALUES (4, 2, 1)
SET IDENTITY_INSERT [dbo].[Authority] OFF
SET IDENTITY_INSERT [dbo].[Bank] ON 

INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (1, 1550024210904, 1)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (2, 1550024221019, 2)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (3, 1550024231085, 3)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (4, 1550024241151, 4)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (5, 1550024251214, 1)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (6, 1550024261277, 2)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (7, 1550024271349, 3)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (8, 1550024281417, 4)
INSERT [dbo].[Bank] ([bankID], [questionID], [answer]) VALUES (9, 1550024447162, 12)
SET IDENTITY_INSERT [dbo].[Bank] OFF
SET IDENTITY_INSERT [dbo].[Page] ON 

INSERT [dbo].[Page] ([pageID], [pageURL], [description]) VALUES (1, N'/takeQuiz', N'Take Quiz')
INSERT [dbo].[Page] ([pageID], [pageURL], [description]) VALUES (2, N'/makeQuiz', N'Make Quiz')
INSERT [dbo].[Page] ([pageID], [pageURL], [description]) VALUES (3, N'/manageQuiz', N'Manage Quiz')
SET IDENTITY_INSERT [dbo].[Page] OFF
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024210904, N'Pronounce of "1"?', N'hailt', CAST(N'2019-02-13T09:16:50.903' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024221019, N'Pronounce of "2"?', N'tuanvm', CAST(N'2019-02-13T09:17:01.020' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024231085, N'Pronounce of "3"?', N'hailt', CAST(N'2019-02-13T09:17:11.087' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024241151, N'Pronounce of "4"?', N'tuanvm', CAST(N'2019-02-13T09:17:21.150' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024251214, N'Pronounce of "5"?', N'tuanvm', CAST(N'2019-02-13T09:17:31.213' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024261277, N'Pronounce of "6"?', N'hailt', CAST(N'2019-02-13T09:17:41.277' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024271349, N'Pronounce of "7"?', N'hailt', CAST(N'2019-02-13T09:17:51.350' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024281417, N'Pronounce of "8"?', N'tuanvm', CAST(N'2019-02-13T09:18:01.417' AS DateTime))
INSERT [dbo].[Question] ([questionID], [questionContent], [username], [dateCreate]) VALUES (1550024447162, N'12<br />12<br />', N'tuanvm', CAST(N'2019-02-13T09:20:47.163' AS DateTime))
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([roleID], [roleName], [description]) VALUES (1, N'Teacher', N'All rights')
INSERT [dbo].[Role] ([roleID], [roleName], [description]) VALUES (2, N'Normal User', N'Take Quiz')
SET IDENTITY_INSERT [dbo].[Role] OFF
ALTER TABLE [dbo].[Account]  WITH CHECK ADD FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[Authority]  WITH CHECK ADD FOREIGN KEY([pageID])
REFERENCES [dbo].[Page] ([pageID])
GO
ALTER TABLE [dbo].[Authority]  WITH CHECK ADD FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[Bank]  WITH CHECK ADD FOREIGN KEY([questionID])
REFERENCES [dbo].[Question] ([questionID])
GO
ALTER TABLE [dbo].[Question]  WITH CHECK ADD FOREIGN KEY([username])
REFERENCES [dbo].[Account] ([username])
GO
ALTER TABLE [dbo].[Result]  WITH CHECK ADD FOREIGN KEY([username])
REFERENCES [dbo].[Account] ([username])
GO
USE [master]
GO
ALTER DATABASE [CMS] SET  READ_WRITE 
GO
